set nu
syntax on
set ruler
colorscheme blink 
set	nobackup
set cin
set smartindent
inoremap ( ()<Left>
inoremap [ []<Left>
inoremap { {<CR>}<Esc>O
set mouse=a
